import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
   
    {
      path: '/homedash',
      name: 'homedash',
      component: () => import('../views/HomeDash.vue')
    },
    {
      path: '/',
      name: 'login',
      component: () => import('../views/LoginView.vue')
    },
    {
      path: '/addproduct',
      name: 'addproduct',
      component: () => import('../views/categorieView.vue')
    }
  ]
})

export default router
